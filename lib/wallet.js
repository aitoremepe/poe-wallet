
// third party libs
const bip39 = require('bip39')
const HDKey = require('hdkey');
const Base64 = require('js-base64').Base64;

//own libs
const walletUtils = require('./util.js')


class Wallet {
    /**
     * Retrieve private key once wallet is initialized.
     * @param {string} mnemonicSeed - Mnemonic Seed.
     */
    constructor(mnemonicSeed){
        this.shake256Length = 28;

        if (mnemonicSeed){
            this.fromMnemonic(mnemonicSeed)
        }
    }

    /**
     * Retrieve private key once wallet is initialized.
     * @param {string} format - Formatting : can be buffer or string.
     * @returns {string} - private key for the given wallet instance.
     */

    privateKey (format = 'buffer') {
        if (!this.instance){
            throw new Error( "Wallet not initialized, cannot retrieve private key" )
        }

        let privateKey = this.instance._privateKey;
        return (format === 'string') ? walletUtils.addHexPrefix( privateKey.toString('hex') ) : privateKey;
    }



    /**
     * Retrieve public key once wallet is initialized.
     * @param {string} format - Formatting : can be buffer or string.
     * @returns {string} - Public key for the given wallet instance.
     */
    publicKey (format = 'buffer') {
        if (!this.instance){
            throw new Error("Wallet not initialized, cannot retrieve public key");
        }

        let publicKey = this.instance._publicKey;
        return (format === 'string') ? walletUtils.addHexPrefix( publicKey.toString('hex') ) : publicKey;
    }


    /**
     * Retrieve address once wallet is initialized.
     * @param {string} format - Formatting : can be buffer or string.
     * @returns {string} - Address for the given wallet instance.
     */

    address (format = 'buffer') {
        if (!this.instance){
            throw new Error( "Wallet not initialized, cannot retrieve address" );
        }

        let publicKeyBuffer = this.instance._publicKey;
        let address = walletUtils.pubToAddress(publicKeyBuffer);

        return (format === 'string') ?  walletUtils.addHexPrefix( address.toString('hex') ) : address;

    }

    /**
     * Initializes privKey, pubKey and address from mnemonic.
     * @param {string} mnemonicSeed - Mnemonic.
     */
    fromMnemonic ( mnemonicSeed ) {
        if (!bip39.validateMnemonic(mnemonicSeed)) {
            throw new Error('Invalid mnemonic provided to Wallet.fromMnemonic :' + mnemonicSeed)
        }

        delete this.mnemonicSeed;
        delete this.instance;

        this.mnemonicSeed = mnemonicSeed
        this.instance = HDKey.fromMasterSeed(mnemonicSeed)
    }

    /**
     * Initializes the current wallet with the given hdkey
     * @param {string} masterMnemonicSeed - Mnemonic.
     */
    fromHDKey (hdkey){
        delete this.mnemonicSeed;
        delete this.instance;
        this.instance = hdkey;
    }

    /**
     * Generates `addressCount` accounts on provided `namespace`
     * @param {string} namespace - Namespace for generation.
     * @param {number} addressCount - How many addresses are to be generated.
     * @returns {Array} - Wallets
     */
    generateAddresses ( namespace, addressCount ){
        let numericNamespace = walletUtils.namespaceToNumber(namespace, this.shake256Length);
        let wallets = [];

        for (var i = 0; i < addressCount; i++){
            let idx = numericNamespace + '' + i;
            let child = this.instance.deriveChild(idx);
            let wallet = new Wallet();
            wallet.fromHDKey(child);
            wallets.push(wallet);

        }
        return wallets;
    }

    /**
     * Generates `addressCount` accounts on provided `namespace`
     * @param {string} namespace - Namespace for generation.
     * @param {number} addressCount - How many addresses are to be generated.
     * @returns {Array} - Addresses
     */
    getAddresses ( namespace, addressCount ) {
        let wallets = this.generateAddresses(namespace, addressCount);
        let addresses = [];

        wallets.forEach( wallet => {
            addresses.push(
                walletUtils.addHexPrefix(
                    wallet.address('string')
                )
            )
        })

        return addresses;
    }


    /**
     * Signs a given message with the current wallet
     * @param {string} messageStr - Message to sign
     * @returns {string} - Base64 encoded json object with hash and signature
     */
    sign  ( messageStr ){
        let hash = walletUtils.hashPersonalMessage(Buffer.from( messageStr ));
        let signed = walletUtils.ecsign( hash, this.privateKey())

        let json = JSON.stringify({
            v : signed.v,
            r : signed.r.toString('hex'),
            s : signed.s.toString('hex'),
            hash : hash.toString('hex')
        });

        return Base64.encode(json);
    }

}




module.exports = Wallet;
