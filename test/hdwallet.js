/* eslint-env mocha */


// const mocha = require('mocha')
const chai = require('chai')
// const should = chai.should()
const assert = chai.assert
const expect = chai.expect
const Wallet = require('../lib/wallet.js');
const util = require('../lib/util.js')
// const path = require('path')

let defaultWallet;

let defaultMnemonic// = 'home october plastic room chief proof raise battle churn hint practice drama';
let defaultNamespace = 'Test name space :D'
let defaultMessage = 'This is a message :D'

let defaultSigned;

describe('HDWallet tests', () => {
    describe('Error coverage on uninitialized wallets', () => {
        it ('Should request privateKey and throw an error since wallet doesn\'t have an instance', () => {
            let errorWallet = new Wallet();
            try{
                errorWallet.privateKey()
            } catch (e){
                expect(e).to.be.a('error')
            }
        })

        it ('Should request a publicKey and throw an error since wallet doesn\'t have an instance', () => {
            let errorWallet = new Wallet();
            try{
                errorWallet.publicKey()
            } catch (e){
                expect(e).to.be.a('error')
            }
        })

        it ('Should request an address and throw an error since wallet doesn\'t have an instance', () => {
            let errorWallet = new Wallet();
            try{
                errorWallet.address()
            } catch (e){
                expect(e).to.be.a('error')
            }
        })

        it ('Should throw an error on invalid mnemonic provided', () => {
            let errorWallet = new Wallet();
            try{
                errorWallet.fromMnemonic('invalidMnemonic :D')
            } catch (e){
                expect(e).to.be.a('error')
            }
        })
    })
    describe('HDWallet util tests', () => {
        it ('Should generate a mnemonic to be used later on tests as buffer', () => {
            let mnemonic = util.createMnemonic('string');
            expect(mnemonic).to.be.a('string');
            assert.isNotEmpty(mnemonic);

            defaultMnemonic = mnemonic;
        })

        it ('Should add "0x" prefix to a given address or key without it', () => {
            let unprefixedKey = '034c4d145791fb81ae5f5cc6b8290e12ab73818b1eaaa42a95c26f488dfcbd6887'
            let prefixed = util.addHexPrefix(unprefixedKey);
            expect(prefixed).to.be.a('string');
            expect(prefixed).to.have.a.lengthOf(unprefixedKey.length + 2);
            expect(prefixed).to.be.equal('0x' + unprefixedKey)
        })

        it ('Should return a "0x" prefixed address or key as is (must NOT add extra "0x")', () => {
            let prefixedKey = '0x034c4d145791fb81ae5f5cc6b8290e12ab73818b1eaaa42a95c26f488dfcbd6887'
            let prefixed = util.addHexPrefix(prefixedKey);
            expect(prefixed).to.be.a('string');
            expect(prefixed).to.have.a.lengthOf(prefixedKey.length);
            expect(prefixed).to.be.equal(prefixedKey)
        })
        /*
        it ('Should verify the defaultSigned using master wallet\'s address', () => {
            let wallet = new Wallet( util.createMnemonic() )
            let signed = wallet.sign(defaultMessage);
            let isValid = util.verifySignature(signed, wallet.address('string'));
            console.log('isValid', isValid)
            expect(signed).to.be.a('string')
        })*/

    })

    describe('HDWallet generation', () => {
        it ('Should create a new wallet instance from a given mnemonic', () => {
            defaultWallet = new Wallet(defaultMnemonic);
            expect(defaultWallet).to.include.keys('mnemonicSeed')
            expect(defaultWallet).to.include.keys('instance')
        })

        it ('Should import a wallet from a given mnemonic as string', () => {
            let mnemonic = util.createMnemonic();
            defaultWallet = new Wallet();
            defaultWallet.fromMnemonic(mnemonic);

            expect(defaultWallet).to.include.keys('mnemonicSeed')
            expect(defaultWallet).to.include.keys('instance')
        })
    })

    describe('HDWallet getters (public and private key, address) as buffer and string', () => {
        it ('Should retrieve the private key for defaultWallet as a buffer', () => {
            let privateKey = defaultWallet.privateKey();
            expect(privateKey).to.be.instanceOf(Buffer);
            assert.isNotEmpty(privateKey);
        })

        it ('Should retrieve the private key for defaultWallet as a string', () => {
            let privateKey = defaultWallet.privateKey('string');
            expect(privateKey).to.be.a('string');
            assert.isNotEmpty(privateKey);
        })

        it ('Should retrieve the public key for defaultWallet as a buffer', () => {
            let publicKey = defaultWallet.publicKey();
            expect(publicKey).to.be.instanceOf(Buffer);
            assert.isNotEmpty(publicKey);
        })

        it ('Should retrieve the public key for defaultWallet as a string', () => {
            let publicKey = defaultWallet.publicKey('string');
            expect(publicKey).to.be.a('string');
            assert.isNotEmpty(publicKey);
        })

        it ('Should retrieve the address for defaultWallet as a buffer', () => {
            let address = defaultWallet.address();
            expect(address).to.be.instanceOf(Buffer);
            assert.isNotEmpty(address);
        })

        it ('Should retrieve the address for defaultWallet as a string', () => {
            let address = defaultWallet.address('string');
            expect(address).to.be.a('string');
            assert.isNotEmpty(address);
        })
    })

    describe('HDWallet address generation', () => {
        it ('Should generate 5 addresses on defaultNamespace', () => {
            let wallets = defaultWallet.generateAddresses(defaultNamespace, 5);
            expect(wallets).to.be.an('array');
            expect(wallets).to.have.a.lengthOf(5);
        })

        it ('Should generate 5 wallet objects on defaultNamespace', () => {
            let wallets = defaultWallet.generateAddresses(defaultNamespace, 5);
            expect(wallets).to.be.an('array');
            expect(wallets).to.have.a.lengthOf(5);
            expect(wallets[0]).to.be.instanceOf(Wallet);
        })

        it ('Should retrieve 5 addresses from defaultNamespace', () => {
            let addresses = defaultWallet.getAddresses(defaultNamespace, 5);
            expect(addresses).to.be.an('array');
            expect(addresses).to.have.a.lengthOf(5);
            expect(addresses[0]).to.be.a('string');
        })
        /*
        it ('Should sign the defaultMessage', () => {
            let signed = defaultWallet.sign(defaultMessage);
            expect(signed).to.be.a('string')
        })
        */
    })

    describe('HDWallet: Sign, verify, encrypt, decrypt', () =>{
        it ('sign using first child on defaultNamespace', () => {
            defaultSigned = defaultWallet.generateAddresses(defaultNamespace, 1)[0].sign(defaultMessage)
            //console.log('addr:', defaultWallet.generateAddresses(defaultNamespace, 1)[0].address('string'))
            expect(defaultSigned).to.be.a('string');
        })

        it('Should verify defaultSigned', () => {
            let isValid = util.verifySignature(defaultSigned, defaultWallet.getAddresses(defaultNamespace, 1)[0])
            //console.log('isValid', isValid)
            expect(isValid).to.be.a('boolean')
            expect(isValid).to.be.equal(true);
        })

    })
})
