# Caelum HDWallet

## Installing
```bash
git clone https://gitlab.com/caelum-labs/poe-wallet/
cd poe-wallet
npm install
```

## Testing
```bash
npm test
```

## Requires
- [BIP39](https://github.com/bitcoinjs/bip39)
- [ethereumjs-util](https://github.com/ethereumjs/ethereumjs-util)
- [JS SHA3 - Shake256](https://github.com/emn178/js-sha3)
- [HDKey](https://github.com/cryptocoinjs/hdkey)
- [Base64](https://github.com/dankogai/js-base64)


## WalletUtils class
- createMnemonic();
- verifySignature( base64Signed, address );
- addHexPrefix( str );
- pubToAddress( publicKeyBuffer );
- namespaceToNumber (namespace, hashLength);
- hashPersonalMessage( stringOrBuffer );
- ecsign (hash, privateKey);

## Wallet class
- constructor( mnemonicSeed );
- privateKey( format );
- publicKey( format );
- address( format );
- fromMnemonic( mnemonicSeed );
- fromHDKey( hdkey );
- generateAddresses( namespace, addressCount );
- getAddresses( namespace, addressCount );
- sign ( messageStr );
